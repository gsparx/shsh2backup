[TSSC] manually specified ecid to use, parsed "1028862431A526" to dec:4548156225332518 hex:1028862431a526
[TSSC] opening firmware.json
[DOWN] downloading file https://api.ipsw.me/v2.1/firmwares.json/condensed
[JSON] counting elements
[JSON] parsing elements
[TSSC] got firmwareurl for iOS 10.2 build 14C92
[TSSC] opening Buildmanifest for iPhone9,1_10.2
[LPZP] downloading BuildManifest.plist from http://appldnld.apple.com/ios10.2seed/031-93637-20161207-617060A6-BBD8-11E6-8DF3-927FE47229E1/iPhone_7_10.2_14C92_Restore.ipsw

[A[J096 [===============================================================================================>    ]
[A[J098 [=================================================================================================>  ]
[A[J100 [===================================================================================================>]
[TSSC] opening bbgcid.json
[DOWN] downloading file http://api.tihmstar.net/bbgcid?condensed=1
[JSON] counting elements
[JSON] parsing elements
[Error] [TSSC] ERROR: device "iPhone9,1" is not in bbgcid.json, which means it's BasebandGoldCertID isn't documented yet.
If you own such a device please consider contacting @tihmstar (tihmstar@gmail.com) to get instructions how to contribute to this project.
[TSSR] WARNING: there was an error getting BasebandGoldCertID, continuing without requesting Baseband ticket
[TSSR] Request URL set to https://gs.apple.com/TSS/controller?action=2
[TSSR] Sending TSS request attempt 1... success
Saved shsh blobs!

iOS 10.2 for device iPhone9,1 IS being signed!
